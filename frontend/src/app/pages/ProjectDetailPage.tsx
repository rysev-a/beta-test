import * as React from 'react';
import ProjectDetail from 'app/components/projects/project-detail/ProjectDetail';

const ProjectDetailPage = () => (
  <div className="section">
    <div className="container">
      <ProjectDetail />
    </div>
  </div>
);

export default ProjectDetailPage;
