import * as React from 'react';
import PlanCreate from 'app/components/plans/plan-create/PlanCreate';

const PlanCreatePage = () => (
  <div className="section">
    <div className="container">
      <PlanCreate />
    </div>
  </div>
);

export default PlanCreatePage;
