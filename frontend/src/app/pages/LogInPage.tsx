import * as React from 'react';
import LogIn from 'app/components/log-in/LogIn';

const LogInPage = () => (
  <div className="section">
    <div className="container">
      <LogIn />
    </div>
  </div>
);

export default LogInPage;
