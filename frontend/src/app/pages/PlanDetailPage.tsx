import * as React from 'react';
import PlantDetail from 'app/components/plans/plan-detail/PlanDetail';

const PlantDetailPage = () => (
  <div className="section">
    <div className="container">
      <PlantDetail />
    </div>
  </div>
);

export default PlantDetailPage;
