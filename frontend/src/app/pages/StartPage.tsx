import * as React from 'react';
import ProjectCreate from 'app/components/projects/project-create/ProjectCreate';

const StartPage = () => (
  <div className="section">
    <div className="container">
      <ProjectCreate />
    </div>
  </div>
);

export default StartPage;
