import * as React from 'react';
import ProjectList from 'app/components/projects/project-list/ProjectList';

const ProjectListPage = () => (
  <div className="section">
    <div className="container">
      <ProjectList />
    </div>
  </div>
);

export default ProjectListPage;
