import * as React from 'react';
import SignUp from 'app/components/sign-up/SignUp';

const SignUpPage = () => (
  <div className="section">
    <div className="container">
      <SignUp />
    </div>
  </div>
);

export default SignUpPage;
