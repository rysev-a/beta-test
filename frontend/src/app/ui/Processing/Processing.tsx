import './Processing.css';
import * as React from 'react';
import classNames from 'classnames';

const Processing = ({ processing }) => (
  <div
    className={classNames({
      processing: true,
      active: processing,
    })}>
    <div className="ball-pulse">
      <div />
      <div />
      <div />
    </div>
  </div>
);

export default Processing;
