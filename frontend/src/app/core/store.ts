import AccountModel from 'app/components/account/AccountModel';
import LogInModel from 'app/components/log-in/LogInModel';
import ProjectCreateModel from 'app/components/projects/project-create/ProjectCreateModel';
import ProjectDetailModel from 'app/components/projects/project-detail/ProjectDetailModel';
import ProjectListModel from 'app/components/projects/project-list/ProjectListModel';
import PlanCreateModel from 'app/components/plans/plan-create/PlanCreateModel';
import SignUpModel from 'app/components/sign-up/SignUpModel';
import PlanDetailModel from 'app/components/plans/plan-detail/PlanDetailModel';

export class Store {
  signUp: SignUpModel;
  logIn: LogInModel;
  account: AccountModel;
  projectCreate: ProjectCreateModel;
  projectList: ProjectListModel;
  projectDetail: ProjectDetailModel;
  planCreate: PlanCreateModel;
  planDetail: PlanDetailModel;

  constructor() {
    this.account = new AccountModel();
    this.signUp = new SignUpModel(this.account);
    this.logIn = new LogInModel(this.account);
    this.projectCreate = new ProjectCreateModel();
    this.projectList = new ProjectListModel();
    this.projectDetail = new ProjectDetailModel(this);
    this.planCreate = new PlanCreateModel();
    this.planDetail = new PlanDetailModel();

    // load account
    this.account.load();
  }
}

export interface InjectStore {
  store: Store;
}

const store = new Store();

export default store;
