const RU = {
  EMAIL_NOT_FOUND: 'Почта не найдена',
  PASSWORD_INCORRECT: 'Неверный пароль',
  EMAIL_WRONG_FORMAT: 'Неверный формат email',
  FIRST_NAME_WRONG_FORMAT: 'Неверный формат имени',
  LAST_NAME_WRONG_FORMAT: 'Неверный формат фамилии',
  EMAIL_IS_ALREADY_EXIST: 'Данный email занят',
  PASSWORD_WRONG_SYMBOLS_COUNT: 'Пароль должен быть не менее 6 символов',

  // projects
  PROJECT_IS_ALREADY_EXIST: 'Проект с таким именем уже существует',
  PROJECT_NAME_IS_EMPTY: 'Необходимо задать название проекта',
};

const EN = {
  EMAIL_NOT_FOUND: 'Email not found',
  EMAIL_WRONG_FORMAT: 'Email wrong format',
  PASSWORD_INCORRECT: 'Password incorrect',
};

const langs = { RU, EN };

const translate = ({ lang = 'RU', text }) => {
  const dict = langs[lang];
  return dict[text] || text;
};

export default translate;
