import axios from 'axios';
import { API_URL } from 'app/settings';

export const devApi = {
  run: (action) => axios.post(`${API_URL}/dev`, { action }),
};
