import { apiFactory } from './factory';

export const userApi = apiFactory('users');
