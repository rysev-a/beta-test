import axios from 'axios';
import { API_URL } from 'app/settings';
import { apiFactory } from './factory';

export const planApi = {
  ...apiFactory('plans'),

  uploadPlan: (formData) => {
    return axios.post(`${API_URL}/plans/create`, formData, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    });
  },
};
