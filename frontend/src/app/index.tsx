import 'bulma/css/bulma.min.css';
import 'normalize.css/normalize.css';
import '@fortawesome/fontawesome-free/css/all.css';

import * as React from 'react';
import { render } from 'react-dom';
import history from 'app/core/history';
import store from 'app/core/store';
import { Provider } from 'mobx-react';
import { Router, Switch, Route } from 'react-router-dom';

import Header from './components/header/Header';
import Dev from './dev/Dev';

import StartPage from './pages/StartPage';
import SignUpPage from './pages/SignUpPage';
import LogInPage from './pages/LogInPage';
import ProjectListPage from './pages/ProjectListPage';
import ProjectDetailPage from './pages/ProjectDetailPage';
import PlanCreatePage from './pages/PlanCreatePage';
import PlanDetailPage from './pages/PlanDetailPage';
import Security from './components/security/Security';

const App = () => {
  return (
    <Provider store={store}>
      <Dev />
      <Router history={history}>
        <Header />
        <Security />
        <Switch>
          <Route path="/plans/create">
            <PlanCreatePage />
          </Route>
          <Route path="/plans/:id">
            <PlanDetailPage />
          </Route>
          <Route path="/projects/:id">
            <ProjectDetailPage />
          </Route>
          <Route path="/projects">
            <ProjectListPage />
          </Route>
          <Route path="/signup">
            <SignUpPage />
          </Route>
          <Route path="/login">
            <LogInPage />
          </Route>
          <Route path="/">
            <StartPage />
          </Route>
        </Switch>
      </Router>
    </Provider>
  );
};

const container = document.getElementById('app');
render(<App />, container);
