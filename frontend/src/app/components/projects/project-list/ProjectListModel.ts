import { projectApi } from 'app/services/projects';
import { makeAutoObservable } from 'mobx';
import { assoc } from 'ramda';
import history from 'app/core/history';

interface Project {
  id: string;
  name: string;
  description: string;
  creator: {
    id: string;
    first_name: string;
    last_name: string;
  };
}

class ProjectListModel {
  constructor() {
    makeAutoObservable(this);
  }

  status = {
    processing: false,
    loaded: false,
  };

  // request props
  sorting = {};
  filters = [];
  pagination = {
    page: 1,
    pages: 0,
  };

  // data props
  items: Project[] = [];

  editProject = (projectId) => history.push(`/projects/${projectId}/edit`);

  removeProject = (projectId) => {
    this.status = assoc('processing', true, this.status);
    projectApi.list
      .delete(projectId)
      .then(() => {
        this.load();
      })
      .catch(() => {
        this.status = assoc('processing', false, this.status);
      });
  };

  reset = () => {
    this.items = [];
    this.pagination = {
      page: 1,
      pages: 0,
    };

    this.status = {
      processing: false,
      loaded: false,
    };
  };

  load = () => {
    this.status = assoc('processing', true, this.status);

    projectApi.list
      .get({
        pagination: this.pagination,
        sorting: this.sorting,
        filters: this.filters,
      })
      .then(({ data: { items } }) => {
        this.items = items;
        this.status = {
          processing: false,
          loaded: true,
        };
      });
  };
}

export default ProjectListModel;
