import ProjectListView from './ProjectListView';
import { inject, observer } from 'mobx-react';
import { InjectStore } from 'app/core/store';

export default inject(({ store }: InjectStore) => ({
  projectList: store.projectList,
}))(observer(ProjectListView));
