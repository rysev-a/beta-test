import * as React from 'react';
import Processing from 'app/ui/Processing/Processing';
import FormControl from 'app/ui/FormControl/FormControl';
import ProjectCreateModel from './ProjectCreateModel';

interface InjectedProps {
  projectCreate: ProjectCreateModel;
}

class ProjectCreateView extends React.Component {
  get injected() {
    return this.props as InjectedProps;
  }

  componentWillUnmount() {
    this.injected.projectCreate.reset();
  }

  render() {
    const { projectCreate: form } = this.injected;

    return (
      <div className="create-project">
        <h1 className="is-size-3  title has-text-weight-normal title has-text-weight-normal">
          Создать новый проект
        </h1>
        <form className="signin-form" onSubmit={form.handleSubmit}>
          <Processing processing={form.isSubmitting} />
          <div className="columns">
            <div className="column">
              <div className="field">
                <label className="label">Название</label>
                <FormControl model={form} field="name" />
              </div>
              <div className="field">
                <label className="label">Описание</label>
                <FormControl
                  model={form}
                  field="description"
                  control="textarea"
                />
              </div>
            </div>
          </div>
          <button
            className="button is-primary"
            type="submit"
            disabled={form.isDisabled}>
            Создать
          </button>
        </form>
      </div>
    );
  }
}

export default ProjectCreateView;
