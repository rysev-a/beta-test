import ProjectCreateView from './ProjectCreateView';
import { inject, observer } from 'mobx-react';
import { InjectStore } from 'app/core/store';

export default inject(({ store }: InjectStore) => ({
  projectCreate: store.projectCreate,
}))(observer(ProjectCreateView));
