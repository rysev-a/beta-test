import history from 'app/core/history';
import { projectApi } from 'app/services/projects';
import { makeAutoObservable } from 'mobx';

class ProjectCreateModel {
  values = {
    name: '',
    description: '',
  };
  errors = {};
  isSubmitting = false;
  processing = false;

  constructor() {
    makeAutoObservable(this);
  }

  handleChange = (e) => {
    this.values[e.target.name] = e.target.value;
    this.resetFieldValidation(e.target.name);
  };

  handleBlur = (e) => {
    const fieldName = e.target.name;
    this.validateField(fieldName);
  };

  handleSubmit = (e) => {
    e.preventDefault();

    projectApi.create
      .post({
        name: this.values.name,
        description: this.values.description,
      })
      .then(() => {
        history.push('/projects');
      })
      .catch(({ response }) => {
        this.processing = false;

        const {
          data: { message },
        } = response;

        this.errors = {
          ...this.errors,
          ...message,
        };
      });
  };

  runValidator({ name, value }) {
    if (name === 'name') {
      if (value === '') {
        return 'PROJECT_NAME_IS_EMPTY';
      }
    }

    return '';
  }

  validateField = (field) => {
    const value = this.values[field];
    this.errors = {
      ...this.errors,
      [`${field}`]: this.runValidator({ name: field, value }) || '',
    };
  };

  validate = () => {
    const validateFields = ['name'];
    validateFields.map((field) => this.validateField(field));
  };

  get isDisabled() {
    return Object.keys(this.errors).some((key: any) => this.errors[key]);
  }

  onSuccess = () => {
    this.isSubmitting = false;
    this.reset();
  };

  reset = () => {
    this.values = {
      name: '',
      description: '',
    };

    this.errors = {
      name: '',
      description: '',
    };
  };

  onError = (error) => {
    const errors = error.response.data.message;
    this.errors = errors;
    this.isSubmitting = false;
  };

  resetFieldValidation = (field) => {
    this.errors = {
      ...this.errors,
      [field]: '',
    };
  };
}

export default ProjectCreateModel;
