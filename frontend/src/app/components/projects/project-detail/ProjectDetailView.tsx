import * as React from 'react';
import { Link } from 'react-router-dom';
import Processing from 'app/ui/Processing/Processing';
import ProjectDetailModel from './ProjectDetailModel';

interface InjectedProps {
  projectDetail: ProjectDetailModel;
  match: {
    params: {
      id: string;
    };
  };
}

class ProjectDetailView extends React.Component {
  get injected() {
    return this.props as InjectedProps;
  }

  componentWillUnmount() {
    this.injected.projectDetail.reset();
  }

  componentDidMount() {
    this.injected.projectDetail.setProjectId(this.injected);
    this.injected.projectDetail.load();
  }

  render() {
    const { projectDetail } = this.injected;

    const {
      data: { name },
      loaded,
      processing,
    } = projectDetail;

    return (
      <div className="project">
        <Processing processing={processing} />

        <section className="hero is-small is-dark">
          <div className="hero-body">
            <div className="container">
              {loaded && (
                <div className="container">
                  <h1 className="is-size-3 title has-text-weight-normal">
                    Проект - {name}
                  </h1>
                </div>
              )}
            </div>
          </div>
        </section>
        <section className="section">
          <div className="container">
            {projectDetail.loaded && projectDetail.data.plans.length === 0 ? (
              <h1>Пока что нет ни одного плана</h1>
            ) : (
              <table className="table is-fullwidth">
                <thead>
                  <tr>
                    <th>Id</th>
                    <th>image</th>
                    <td>Координаты</td>
                    <th>Действия</th>
                  </tr>
                </thead>

                <tbody>
                  {projectDetail.data.plans.map(
                    ({ id, image, left, top, right, bottom }) => (
                      <tr key={id}>
                        <th>
                          <Link to={`/plans/${id}`}>План #{id}</Link>
                        </th>
                        <td>{image}</td>
                        <td>
                          {left}/{top} - {right}/{bottom}
                        </td>
                        <td>
                          <div className="is-grouped field">
                            <div className="control">
                              <a
                                className="button is-danger is-small"
                                onClick={() => {
                                  projectDetail.removePlan(id);
                                }}>
                                Удалить
                              </a>
                            </div>
                          </div>
                        </td>
                      </tr>
                    )
                  )}
                </tbody>
              </table>
            )}
          </div>
        </section>
        <div className="buttons">
          <Link className="button is-success" to="/plans/create">
            Добавить план
          </Link>
        </div>
      </div>
    );
  }
}

export default ProjectDetailView;
