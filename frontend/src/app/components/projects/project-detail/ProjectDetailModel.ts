import { makeAutoObservable } from 'mobx';
import { projectApi } from 'app/services/projects';
import { Store } from 'app/core/store';
import { planApi } from 'app/services/plans';

const defaultProjectDetailData = () => ({
  id: 0,
  name: '',
  description: '',
  creator: {
    first_name: '',
    last_name: '',
  },
  plans: [],
});

class ProjectDetailModel {
  store: Store;
  constructor(store) {
    this.store = store;
    makeAutoObservable(this);
  }

  removePlan = (id) => {
    planApi.list.delete(id).then(() => {
      this.load();
    });
  };

  loaded = false;
  processing = false;
  data = defaultProjectDetailData();
  projectId = 0;

  setProjectId = ({
    match: {
      params: { id },
    },
  }) => {
    this.projectId = id;
    this.store.planCreate.setProjectId(this.projectId);
  };

  load = () => {
    this.processing = true;
    return projectApi.detail
      .get(this.projectId)
      .then(this.onSuccess)
      .catch(() => {
        this.processing = false;
      });
  };

  onSuccess = ({ data }) => {
    this.data = data;
    this.loaded = true;
    this.processing = false;
  };

  reset = () => {
    this.data = defaultProjectDetailData();
    this.loaded = false;
  };
}

export default ProjectDetailModel;
