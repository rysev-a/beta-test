import PlanDetailView from './PlanDetailView';
import { inject, observer } from 'mobx-react';
import { InjectStore } from 'app/core/store';

import { withRouter } from 'react-router-dom';

export default withRouter(
  inject(({ store }: InjectStore) => ({
    planDetail: store.planDetail,
  }))(observer(PlanDetailView))
);
