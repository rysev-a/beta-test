import './Plan.css';

import { UPLOAD_URL } from 'app/settings';
import * as React from 'react';
import Processing from 'app/ui/Processing/Processing';
import PlanDetailModel from './PlanDetailModel';
import * as leaflet from 'leaflet';

interface InjectedProps {
  planDetail: PlanDetailModel;
  match: {
    params: {
      id: string;
    };
  };
}

class ProjectDetailView extends React.Component {
  map: leaflet;
  get injected() {
    return this.props as InjectedProps;
  }

  componentWillUnmount() {
    this.injected.planDetail.reset();
  }

  componentDidMount() {
    this.injected.planDetail.setPlanId(this.injected);
    this.injected.planDetail.load().then(() => {
      this.renderMap();
    });
  }

  addPlan() {
    const { image, left, top, right, bottom } = this.injected.planDetail.data;

    const leftTop = [top, left];
    const rightBottom = [bottom, right];

    const bounds = [leftTop, rightBottom];
    const imageUrl = `${UPLOAD_URL}${image}`;

    leaflet.imageOverlay(imageUrl, bounds).addTo(this.map);

    const polygon = leaflet
      .polygon([
        [top, left],
        [top, right],
        [bottom, right],
        [bottom, left],
      ])
      .addTo(this.map);

    polygon.addEventListener('mousemove', (event) => {
      this.injected.planDetail.setLatLng(event.latlng);
    });
    polygon.addEventListener('mouseout', () => {
      console.log('out');
      this.injected.planDetail.setLatLng({ lat: 0, lng: 0 });
    });
  }

  addMainLayer() {
    leaflet
      .tileLayer(
        'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw',
        {
          maxZoom: 18,
          attribution:
            'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
            '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
            'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
          id: 'mapbox/streets-v11',
          tileSize: 512,
          zoomOffset: -1,
        }
      )
      .addTo(this.map);
  }

  renderMap() {
    const { left, top, right, bottom } = this.injected.planDetail.data;

    const y = (top + bottom) / 2;
    const x = (left + right) / 2;

    this.map = leaflet.map('mapid').setView([y, x], 17);
    this.addMainLayer();
    this.addPlan();
  }

  render() {
    const { planDetail } = this.injected;

    const {
      data: { id },
      loaded,
      processing,
    } = planDetail;

    return (
      <div className="plan">
        <Processing processing={processing} />

        <section className="hero is-small is-dark">
          <div className="hero-body">
            <div className="container">
              {loaded && (
                <h1 className="is-size-3 title has-text-weight-normal">
                  План - {id}
                </h1>
              )}
            </div>
          </div>
        </section>

        <section>
          <div className="container">
            <div id="mapid" className="plan-map"></div>
          </div>
        </section>

        {planDetail.lat !== 0 && planDetail.lng !== 0 && (
          <div className="container">
            <table className="table">
              <thead>
                <tr>
                  <th className="table-item">Широта</th>
                  <th className="table-item">Долгота</th>
                </tr>
              </thead>

              <tbody>
                <tr>
                  <td>{planDetail.lat}</td>
                  <td>{planDetail.lng}</td>
                </tr>
              </tbody>
            </table>
          </div>
        )}
      </div>
    );
  }
}

export default ProjectDetailView;
