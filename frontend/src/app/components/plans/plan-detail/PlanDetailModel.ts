import { makeAutoObservable } from 'mobx';
import { planApi } from 'app/services/plans';

export const defaultPlanData = () => ({
  id: 0,
  project_id: 0,
  left: 0,
  top: 0,
  right: 0,
  bottom: 0,
  image: '',
});

class PlanDetailModel {
  constructor() {
    makeAutoObservable(this);
  }

  loaded = false;
  processing = false;
  data = defaultPlanData();
  planId = 0;

  lat = 0;
  lng = 0;

  setPlanId = ({
    match: {
      params: { id },
    },
  }) => {
    this.planId = id;
  };

  load = () => {
    this.processing = true;

    return planApi.detail
      .get(this.planId)
      .then(this.onSuccess)
      .catch(() => {
        this.processing = false;
      });
  };

  setLatLng = ({ lat, lng }) => {
    this.lat = lat;
    this.lng = lng;

    console.log(this.lat);
  };

  onSuccess = ({ data }) => {
    this.data = data;
    this.loaded = true;
    this.processing = false;
  };

  reset = () => {
    this.data = defaultPlanData();
    this.loaded = false;
  };
}

export default PlanDetailModel;
