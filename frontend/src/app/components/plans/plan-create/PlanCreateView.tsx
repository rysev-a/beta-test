import * as React from 'react';
import Processing from 'app/ui/Processing/Processing';
import FormControl from 'app/ui/FormControl/FormControl';
import PlanCreateModel from './PlanCreateModel';
import history from 'app/core/history';

interface InjectedProps {
  planCreate: PlanCreateModel;
}

class ProjectCreateView extends React.Component {
  get injected() {
    return this.props as InjectedProps;
  }

  componentDidMount() {
    if (!this.injected.planCreate.values.project_id) {
      history.push('/');
    }
  }

  componentWillUnmount() {
    this.injected.planCreate.reset();
  }

  render() {
    const { planCreate: form } = this.injected;

    return (
      <div className="create-project">
        <h1 className="is-size-3  title has-text-weight-normal title has-text-weight-normal">
          Создать новый план
        </h1>
        <form className="signin-form" onSubmit={form.handleSubmit}>
          <Processing processing={form.isSubmitting} />
          <div className="columns">
            <div className="column">
              <div className="field">
                <label className="label">Project ID</label>
                <input
                  className="input"
                  value={form.values.project_id}
                  disabled
                />
              </div>
              <div className="field">
                <label className="label">Первая точка</label>
                <FormControl model={form} field="left" />
                <FormControl model={form} field="top" />
              </div>
              <div className="field">
                <label className="label">Вторая точка</label>
                <FormControl model={form} field="right" />
                <FormControl model={form} field="bottom" />
              </div>

              <div className="file">
                <label className="file-label">
                  <input
                    className="file-input"
                    onChange={form.uploadImage}
                    type="file"
                    name="resume"
                  />
                  <span className="file-cta">
                    <span className="file-icon">
                      <i className="fas fa-upload"></i>
                    </span>
                    <span className="file-label">Изображение плана</span>
                  </span>
                </label>
              </div>
            </div>
          </div>
          <button
            className="button is-primary"
            type="submit"
            disabled={form.isDisabled}>
            Создать
          </button>
        </form>
      </div>
    );
  }
}

export default ProjectCreateView;
