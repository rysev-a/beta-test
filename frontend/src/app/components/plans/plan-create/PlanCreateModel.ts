import history from 'app/core/history';
import { planApi } from 'app/services/plans';
import { makeAutoObservable } from 'mobx';

import { defaultPlanData } from '../plan-detail/PlanDetailModel';

class ProjectCreateModel {
  values = {
    project_id: 2,
    left: 0,
    top: 0,
    right: 0,
    bottom: 0,
    image: '',
  };
  errors = {};
  isSubmitting = false;
  processing = false;

  constructor() {
    makeAutoObservable(this);
  }

  setProjectId(project_id) {
    this.values.project_id = project_id;
  }

  handleChange = (e) => {
    this.values[e.target.name] = e.target.value;
    this.resetFieldValidation(e.target.name);
  };

  handleBlur = (e) => {
    const fieldName = e.target.name;
    this.validateField(fieldName);
  };

  handleSubmit = (e) => {
    e.preventDefault();
    const formData = new FormData();

    formData.append('left', String(this.values.left));
    formData.append('top', String(this.values.top));
    formData.append('right', String(this.values.right));
    formData.append('bottom', String(this.values.bottom));
    formData.append('project_id', String(this.values.project_id));
    formData.append('image', this.values.image);

    planApi
      .uploadPlan(formData)
      .then((response) => {
        const { id } = response.data;
        history.push(`/plans/${id}`);
      })
      .catch(({ response }) => {
        this.processing = false;

        const {
          data: { message },
        } = response;

        this.errors = {
          ...this.errors,
          ...message,
        };
      });
  };

  runValidator({ name, value }) {
    return '';
  }

  validateField = (field) => {
    const value = this.values[field];
    this.errors = {
      ...this.errors,
      [`${field}`]: this.runValidator({ name: field, value }) || '',
    };
  };

  validate = () => {
    const validateFields = [];
    validateFields.map((field) => this.validateField(field));
  };

  get isDisabled() {
    return Object.keys(this.errors).some((key: any) => this.errors[key]);
  }

  onSuccess = () => {
    this.isSubmitting = false;
    this.reset();
  };

  reset = () => {
    this.values = defaultPlanData();

    this.errors = {};
  };

  onError = (error) => {
    const errors = error.response.data.message;
    this.errors = errors;
    this.isSubmitting = false;
  };

  uploadImage = (e) => {
    this.values.image = e.target.files[0];
  };

  resetFieldValidation = (field) => {
    this.errors = {
      ...this.errors,
      [field]: '',
    };
  };
}

export default ProjectCreateModel;
