import PlanCreateView from './PlanCreateView';
import { inject, observer } from 'mobx-react';
import { InjectStore } from 'app/core/store';

export default inject(({ store }: InjectStore) => ({
  planCreate: store.planCreate,
}))(observer(PlanCreateView));
