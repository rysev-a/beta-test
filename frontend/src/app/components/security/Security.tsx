import * as React from 'react';
import { withRouter } from 'react-router-dom';
import { inject, observer } from 'mobx-react';
import history from 'app/core/history';
import { InjectStore } from 'app/core/store';

interface InjectedProps {
  isAuth: boolean;
  loaded: boolean;
  location: {
    pathname: string;
  };
}

class Security extends React.Component {
  allowedPathnames = ['/signup', '/login'];
  get injected() {
    return this.props as InjectedProps;
  }

  redirectIfNeeded() {
    if (!this.injected.loaded) {
      return false;
    }

    if (this.injected.isAuth) {
      return false;
    }

    console.log('check redirect!!!');

    const { pathname } = this.injected.location;
    if (this.allowedPathnames.includes(pathname)) {
      return false;
    }

    history.push('/login');
  }

  componentDidMount() {
    this.redirectIfNeeded();
  }

  componentDidUpdate() {
    this.redirectIfNeeded();
  }

  render() {
    return null;
  }
}

export default withRouter(
  inject(({ store }: InjectStore) => ({
    isAuth: store.account.isAuth,
    loaded: store.account.loaded,
  }))(observer(Security))
);
