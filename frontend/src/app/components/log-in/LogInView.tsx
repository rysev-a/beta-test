import * as React from 'react';
import Processing from 'app/ui/Processing/Processing';
import LogInModel from './LogInModel';
import FormControl from 'app/ui/FormControl/FormControl';

interface InjectedProps {
  logIn: LogInModel;
}

class LogInView extends React.Component {
  constructor(props) {
    super(props);
  }

  get injected() {
    return this.props as InjectedProps;
  }

  render() {
    const { logIn } = this.injected;
    const { handleSubmit, isSubmitting, isDisabled } = logIn;

    return (
      <div className="level">
        <div className="level-item">
          <section className="section">
            <div className="level">
              <h2 className="is-size-4 is-center level-item">Вход</h2>
            </div>
            <form className="signin-form" onSubmit={handleSubmit}>
              <Processing processing={isSubmitting} />

              <div className="field">
                <label className="label">Email</label>
                <FormControl model={logIn} field="email" type="email" />
              </div>

              <div className="field">
                <label className="label">Password</label>
                <FormControl model={logIn} field="password" type="password" />
              </div>

              <button className="button" type="submit" disabled={isDisabled}>
                Войти
              </button>
            </form>
          </section>
        </div>
      </div>
    );
  }
}

export default LogInView;
