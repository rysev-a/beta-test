import LogInView from './LogInView';
import { inject, observer } from 'mobx-react';

export default inject(({ store }) => ({
  logIn: store.logIn,
}))(observer(LogInView));
