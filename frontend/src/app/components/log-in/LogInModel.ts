import { makeAutoObservable } from 'mobx';
import { validate } from 'app/core/utils/validators';
import { accountApi } from 'app/services/account';
import history from 'app/core/history';
import AccountModel from '../account/AccountModel';

interface LogInForm {
  values: {
    email: string;
    password: string;
  };
  handleChange(event): void;
  handleBlur(event): void;
  validateField(field): void;
  validate(): void;
  handleSubmit(event): void;
  onSuccess(): void;
  onError(error): void;
}

class LogInModel implements LogInForm {
  account: AccountModel;

  constructor(account) {
    this.account = account;
    makeAutoObservable(this);
  }
  values = {
    email: '',
    password: '',
  };
  errors = {};
  isSubmitting = false;

  handleChange = (e) => {
    this.values[e.target.name] = e.target.value;
    this.resetFieldValidation(e.target.name);
  };

  handleSubmit = (e) => {
    e.preventDefault();
    this.validate();

    if (this.isDisabled) {
      return false;
    }

    accountApi
      .signin({
        email: this.values.email,
        password: this.values.password,
      })
      .then(() => this.onSuccess())
      .catch((error) => this.onError(error));
  };

  handleBlur = (e) => {
    const fieldName = e.target.name;
    this.validateField(fieldName);
  };

  validateField = (field) => {
    const value = this.values[field];
    this.errors = {
      ...this.errors,
      [`${field}`]: validate({ name: field, value }) || '',
    };
  };

  validate = () => {
    const validateFields = ['email'];
    validateFields.map((field) => this.validateField(field));
  };

  get isDisabled() {
    return Object.keys(this.errors).some((key: any) => this.errors[key]);
  }

  onSuccess = () => {
    this.isSubmitting = false;
    this.reset();
    this.account.load().then(() => {
      history.push('/');
    });
  };

  reset = () => {
    this.values = {
      email: '',
      password: '',
    };
  };

  onError = (error) => {
    const errors = error.response.data.message;
    this.errors = errors;
    this.isSubmitting = false;
  };

  resetFieldValidation = (field) => {
    this.errors = {
      ...this.errors,
      [field]: '',
    };
  };
}

export default LogInModel;
