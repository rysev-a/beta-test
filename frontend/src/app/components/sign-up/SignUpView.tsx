import * as React from 'react';
import Processing from 'app/ui/Processing/Processing';
import SignUpModel from './SignUpModel';
import FormControl from 'app/ui/FormControl/FormControl';

interface InjectedProps {
  signUp: SignUpModel;
}

class SignUpView extends React.Component {
  constructor(props) {
    super(props);
  }

  get injected() {
    return this.props as InjectedProps;
  }

  render() {
    const { signUp } = this.injected;
    const { isSubmitting, handleSubmit, isDisabled } = signUp;

    return (
      <div className="level">
        <div className="level-item">
          <section className="section">
            <div className="level">
              <h2 className="is-size-4 is-center level-item">Регистрация</h2>
            </div>
            <form className="signin-form">
              <Processing processing={isSubmitting} />
              <div className="field">
                <label className="label">Имя</label>
                <FormControl model={signUp} field="first_name" />
              </div>

              <div className="field">
                <label className="label">Фамилия</label>
                <FormControl model={signUp} field="last_name" />
              </div>

              <div className="field">
                <label className="label">Email</label>
                <FormControl model={signUp} type="email" field="email" />
              </div>

              <div className="field">
                <label className="label">Password</label>
                <FormControl model={signUp} type="password" field="password" />
              </div>

              <button
                className="button is-info is-fullwidth"
                onClick={handleSubmit}
                disabled={isDisabled}
                type="submit">
                Отправить
              </button>
            </form>
          </section>
        </div>
      </div>
    );
  }
}

export default SignUpView;
