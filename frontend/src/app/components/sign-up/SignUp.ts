import SignUpView from './SignUpView';
import { inject, observer } from 'mobx-react';

export default inject(({ store }) => ({
  signUp: store.signUp,
}))(observer(SignUpView));
