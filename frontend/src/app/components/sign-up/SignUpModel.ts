import history from 'app/core/history';
import { makeAutoObservable } from 'mobx';

import { validate } from 'app/core/utils/validators';
import { accountApi } from 'app/services/account';
import AccountModel from '../account/AccountModel';

class SignUpModel {
  account: AccountModel;

  constructor(account) {
    makeAutoObservable(this);
    this.account = account;
  }

  values = {
    first_name: '',
    last_name: '',
    email: '',
    password: '',
  };
  isSubmitting = false;
  errors = {
    email: '',
    password: '',
    first_name: '',
    last_name: '',
  };

  defaultSignUpValues = () => {
    return {
      first_name: '',
      last_name: '',
      email: '',
      password: '',
    };
  };

  handleChange = (e) => {
    this.values[e.target.name] = e.target.value;
    this.resetFieldValidation(e.target.name);
  };

  handleBlur = (e) => {
    const fieldName = e.target.name;
    this.validateField(fieldName);
  };

  get isDisabled() {
    return Object.keys(this.errors).some((key: any) => this.errors[key]);
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.validate();

    if (this.isDisabled) {
      return false;
    }

    accountApi
      .signup(this.serialize())
      .then(() => {
        this.onSuccess();
      })
      .catch(({ response }) => {
        this.onError(response);
      });
  };

  reset = () => {
    this.values = this.defaultSignUpValues();
  };

  resetFieldValidation = (field) => {
    this.errors = {
      ...this.errors,
      [field]: '',
    };
  };

  validateField = (field) => {
    const value = this.values[field];
    this.errors = {
      ...this.errors,
      [`${field}`]: validate({ name: field, value }) || '',
    };
  };

  validate = () => {
    const validateFields = ['email', 'first_name', 'last_name', 'password'];
    validateFields.map((field) => this.validateField(field));
  };

  serialize = () => {
    return {
      first_name: this.values.first_name,
      last_name: this.values.last_name,
      email: this.values.email,
      password: this.values.password,
    };
  };

  onError = (response) => {
    this.isSubmitting = false;

    const {
      data: { message },
    } = response;

    this.errors = {
      ...this.errors,
      ...message,
    };
  };

  onSuccess = () => {
    this.isSubmitting = false;
    this.reset();
    this.account.load().then(() => {
      history.push('/');
    });
  };
}

export default SignUpModel;
