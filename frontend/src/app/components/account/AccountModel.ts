import { makeAutoObservable } from 'mobx';
import { accountApi } from 'app/services/account';
import history from 'app/core/history';

interface AccountModelData {
  data: {};
  processing: boolean;
  loaded: boolean;
  isAuth: boolean;

  load(callback): void;
  signout(): void;
}

const defaultAccountData = () => ({
  email: '',
  first_name: '',
  last_name: '',
});

class AccountModel implements AccountModelData {
  constructor() {
    makeAutoObservable(this);
  }

  data = defaultAccountData();
  processing = false;
  isAuth = false;
  loaded = false;

  load = () => {
    this.processing = true;

    return accountApi
      .load()
      .then(({ data }) => {
        this.data = {
          ...data,
        };
        this.isAuth = true;
        this.loaded = true;
        this.processing = false;
        // this.initializeAccountFormSettings();

        return new Promise((resolve) => {
          resolve();
        });
      })
      .catch(() => {
        this.loaded = true;
        this.processing = false;
        this.isAuth = false;
      });
  };

  signout = () => {
    this.processing = true;

    accountApi
      .signout()
      .then(() => {
        this.processing = false;
        this.reset();
        history.push('/');
      })
      .catch(() => {
        this.processing = false;
        this.reset();
      });
  };

  reset = () => {
    this.data = defaultAccountData();
    this.isAuth = false;
  };

  initializeAccountFormSettings() {
    // accountSettings.initialize(this.data);
    // accountSettings.syncAccountModel = (data) => {
    //   this.data = data;
    // };
  }
}

export default AccountModel;
