import './AccountMenu.css';
import * as React from 'react';

import { Link } from 'react-router-dom';
import { inject, observer } from 'mobx-react';
import AccountModel from '../account/AccountModel';

interface InjectedProps {
  account: AccountModel;
}

class AccountMenuView extends React.Component {
  constructor(props) {
    super(props);
  }

  get injected() {
    return this.props as InjectedProps;
  }

  render() {
    const { account } = this.injected;

    if (account.processing) {
      return null;
    }

    return (
      <div className="navbar-end account-menu">
        {account.isAuth && (
          <React.Fragment>
            <Link className="navbar-item" to="/projects">
              Список проектов
            </Link>

            <div
              className="navbar-item has-dropdown is-hoverable"
              id="account-dropdown">
              <a className="navbar-link">{account.data.email}</a>
              <div className="navbar-dropdown account-menu-dropdown">
                <div className="sign-menu">
                  <a className="navbar-item" onClick={account.signout}>
                    Выйти
                  </a>
                </div>
              </div>
            </div>
          </React.Fragment>
        )}
        {!account.isAuth && (
          <>
            <Link className="navbar-item" to="/login">
              Вход
            </Link>
            <Link className="navbar-item" to="/signup">
              Регистрация
            </Link>
          </>
        )}
      </div>
    );
  }
}

export default inject(({ store }) => ({
  account: store.account,
}))(observer(AccountMenuView));
