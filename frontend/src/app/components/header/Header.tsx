import './Header.css';
import * as React from 'react';

import { Link } from 'react-router-dom';
import AccountMenu from './AccountMenu';

const Header = () => {
  return (
    <header className="header">
      <div className="container">
        <nav className="navbar">
          <Link className="navbar-brand is-size-4" to="/">
            Beta
          </Link>
          <AccountMenu />
        </nav>
      </div>
    </header>
  );
};

export default Header;
