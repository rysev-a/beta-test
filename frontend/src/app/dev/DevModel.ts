import { devApi } from 'app/services/dev';

class DevModel {
  run(action) {
    devApi.run(action).then((response) => {
      console.log(response.data);
    });
  }
}

export default DevModel;
