import * as React from 'react';
import DevModel from './DevModel';
import DevView from './DevVIew';

const devModel = new DevModel();

const Dev = () => {
  return <DevView devModel={devModel} />;
};

export default Dev;
