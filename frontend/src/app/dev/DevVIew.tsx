import './Dev.css';
import * as React from 'react';

const DevView = ({ devModel }) => {
  return (
    <div className="dev-panel">
      <div className="card">
        <div className="card-content">
          <h2
            className="is-title is-size-3 has-text-weight-light
          has-text-centered
          ">
            dev panel
          </h2>
          <div className="buttons">
            <a
              className="button is-small is-fullwidth"
              onClick={() => {
                devModel.run('clear');
              }}>
              clear database
            </a>
            <a
              className="button is-small is-fullwidth"
              onClick={() => {
                devModel.run('load_roles');
              }}>
              load roles
            </a>
            <a
              className="button is-small is-fullwidth"
              onClick={() => {
                devModel.run('load_users');
              }}>
              load users
            </a>
            <a
              className="button is-small is-fullwidth"
              onClick={() => {
                devModel.run('load_projects');
              }}>
              load projects
            </a>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DevView;
