import os
import click
from app import create_app
from app.core.database import db

from app.services.users.models import User, Role
from app.services.projects.models import Project

myenv = os.environ.get(
    'SETTINGS_ENV',
    'app.settings.development'
)

print(myenv)


app = create_app(os.environ.get(
    'SETTINGS_ENV',
    'app.settings.development'
))


@app.cli.command()
def clear():
    db.drop_all()
    db.create_all()


@app.shell_context_processor
def make_shell_context():
    return dict(
        app=app,
        db=db,
        User=User,
        Role=Role,
        Project=Project,
    )
