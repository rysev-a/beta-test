DEBUG = True
SECRET_KEY = 'secret_keyWiTh$ymbols'

DB_NAME = 'database_name'
DB_USER = 'database_user'
DB_PASSWORD = 'database_password'
DB_PORT = '5432'
DB_HOST = 'localhost'
DB_ENGINE = 'postgresql'

SQLALCHEMY_DATABASE_URI = f'{DB_ENGINE}://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}'
SQLALCHEMY_TRACK_MODIFICATIONS = True

UPLOAD_FOLDER = '/home/user/Projects/beta-test/upload'
