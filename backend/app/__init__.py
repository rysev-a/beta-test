from flask import Flask
from app.core.database import db
from app.core.api import api
from app.core.migrate import migrate
from app.core.auth import login_manager
from app.services import init_services


def create_app(settings='app.settings.development'):
    app = Flask(__name__)
    app.config.from_object(settings)

    # init core (db, migrations, api, auth)
    db.init_app(app)
    migrate.init_app(app, db)
    login_manager.init_app(app)

    # init services
    init_services()
    api.init_app(app)

    return app
