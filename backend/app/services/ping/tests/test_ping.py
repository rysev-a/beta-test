def test_ping(client):
    request = client.get('/api/v1/ping', content_type='application/json')
    assert request.json.get('pong') == 'ok'
    assert request.status == '200 OK'
