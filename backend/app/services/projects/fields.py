from flask_restful import fields

user_fields = {
    'id': fields.Integer,
    'email': fields.String,
    'first_name': fields.String,
    'last_name': fields.String,
}


plan_detail_fields = {
    'id': fields.Integer,
    'top': fields.Float,
    'left': fields.Float,
    'right': fields.Float,
    'bottom': fields.Float,
    'image': fields.String,
}

plan_list_fields = {
    'id': fields.Integer,
    'top': fields.Float,
    'left': fields.Float,
    'right': fields.Float,
    'bottom': fields.Float,
    'image': fields.String,
}


project_list_fields = {
    'id': fields.Integer,
    'name': fields.String,
    'description': fields.String,
    'creator': fields.Nested(user_fields)
}

project_detail_fields = {
    'id': fields.Integer,
    'name': fields.String,
    'description': fields.String,
    'creator': fields.Nested(user_fields),
    'users': fields.List(
        fields.Nested(user_fields)
    ),
    'plans': fields.List(
        fields.Nested(plan_detail_fields)
    ),
}
