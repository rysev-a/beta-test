import os
import uuid
from werkzeug.utils import secure_filename
from flask_restful import Resource, request, marshal
from flask_login import current_user
from flask import current_app

from app.core.crud import ListResource, DetailResource
from app.core.database import db
from .fields import (
    project_detail_fields,
    project_list_fields,
    plan_detail_fields,
    plan_list_fields
)
from .models import Project, Plan, users_projects_association


class PlanDetail(DetailResource):
    fields = plan_detail_fields
    model = Plan


class PlanList(ListResource):
    fields = plan_list_fields
    model = Plan


class ProjectList(ListResource):
    fields = project_list_fields
    model = Project


class ProjectDetail(DetailResource):
    fields = project_detail_fields
    model = Project


class ProjectCreate(Resource):
    def post(self):
        exist_project = Project.query.filter_by(
            name=request.json['name']).first()
        if exist_project:
            return {'message': {
                'name': 'PROJECT_IS_ALREADY_EXIST'
            }}, 400

        user_ids = request.json.get('users', [])
        if request.json.get('users'):
            del request.json['users']

        project = Project(
            creator_id=current_user.id,
            name=request.json.get('name'),
            description=request.json.get('description')
        )

        db.session.add(project)
        db.session.commit()

        if len(user_ids) > 0:
            project.add_users(user_ids)

        return marshal(project, project_detail_fields)


class ProjectUpdate(Resource):
    def put(self, id):
        query = Project.query.filter_by(id=id)
        project = Project.query.get(id)

        self.update_users(request.json, project)

        query.update(request.json)
        db.session.commit()
        return marshal(query.first(), project_detail_fields)

    @staticmethod
    def update_users(request_json, project):
        user_ids = request_json['users']
        prev_user_ids = [user.id for user in project.users]

        removed_user_ids = list(set(prev_user_ids) - set(user_ids))
        add_users_ids = list(set(user_ids) - set(prev_user_ids))

        project.remove_users(removed_user_ids)
        project.add_users(add_users_ids)

        del request_json['users']


class PlanCreate(Resource):
    def post(self):
        left = request.form['left']
        top = request.form['top']
        right = request.form['right']
        bottom = request.form['bottom']

        project_id = request.form['project_id']

        upload_image = request.files.get('image')
        upload_path = current_app.config.get('UPLOAD_FOLDER')

        _, file_extension = os.path.splitext(upload_image.filename)
        filename = f'{str(uuid.uuid4())}{file_extension}'
        upload_image.save(os.path.join(
            upload_path,  filename))

        plan = Plan(
            left=left,
            top=top,
            right=right,
            bottom=bottom,
            project_id=project_id,
            image=filename
        )

        db.session.add(plan)
        db.session.commit()

        return marshal(plan, plan_detail_fields)
