from app.core.api import api

from .ping.resources import Ping
from .users.resources import UserList, UserDetail, RoleList, RoleDetail
from .projects.resources import (
    ProjectDetail,
    ProjectList,
    ProjectCreate,
    ProjectUpdate,

    PlanDetail,
    PlanCreate,
    PlanList
)

from .account.resources import (
    AccountSignin,
    AccountSignup,
    AccountSignout,
    AccountUpdate,
    AccountInfo
)

from .dev.resources import DevResource


def init_services():
    api.add_resource(Ping, '/api/v1/ping')

    api.add_resource(UserList, '/api/v1/users')
    api.add_resource(UserDetail, '/api/v1/users/<int:id>')

    api.add_resource(RoleList, '/api/v1/roles')
    api.add_resource(RoleDetail, '/api/v1/roles/<int:id>')

    api.add_resource(ProjectList, '/api/v1/projects')
    api.add_resource(ProjectDetail, '/api/v1/projects/<int:id>')
    api.add_resource(ProjectUpdate, '/api/v1/projects/<int:id>/update')
    api.add_resource(ProjectCreate, '/api/v1/projects/create')

    api.add_resource(PlanCreate, '/api/v1/plans/create')
    api.add_resource(PlanDetail, '/api/v1/plans/<int:id>')
    api.add_resource(PlanList, '/api/v1/plans')

    api.add_resource(AccountInfo, '/api/v1/account')
    api.add_resource(AccountSignin, '/api/v1/account/signin')
    api.add_resource(AccountSignup, '/api/v1/account/signup')
    api.add_resource(AccountUpdate, '/api/v1/account/update')
    api.add_resource(AccountSignout, '/api/v1/account/signout')

    api.add_resource(DevResource, '/api/v1/dev')
