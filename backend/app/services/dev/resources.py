import os
import pathlib
import json
import yaml
from sqlalchemy import desc, asc
from flask import current_app
from flask_restful import marshal, request, reqparse, Resource, fields
from app.core.database import db
from app.services.users.models import User, Role
from app.services.projects.models import Project


def clear():
    db.drop_all()
    db.create_all()


def load_roles():
    current_path = pathlib.Path(__file__).parent.absolute()
    mock_path = f'{current_path}/fixtures/roles.yaml'

    with open(mock_path) as mock_data:
        for model_data in yaml.load(mock_data, Loader=yaml.FullLoader):
            db.session.add(Role(**model_data))
        db.session.commit()


def load_projects():
    current_path = pathlib.Path(__file__).parent.absolute()
    mock_path = f'{current_path}/fixtures/projects.yaml'

    user_id = User.query.all()[0].id
    with open(mock_path) as mock_data:
        for model_data in yaml.load(mock_data, Loader=yaml.FullLoader):
            db.session.add(Project(**{
                "name": model_data.get("name"),
                "description": model_data.get("description"),
                "creator_id": user_id,
            }))
        db.session.commit()


def load_users():
    current_path = pathlib.Path(__file__).parent.absolute()
    mock_path = f'{current_path}/fixtures/users.yaml'

    with open(mock_path) as mock_data:
        for model_data in yaml.load(mock_data, Loader=yaml.FullLoader):
            role_id = Role.query.filter_by(
                name=model_data.get("role")).first().id

            db.session.add(User(
                email=model_data.get("email"),
                first_name=model_data.get("first_name"),
                last_name=model_data.get("last_name"),
                password=model_data.get("password"),
                role_id=role_id
            ))
        db.session.commit()


actions = {
    "load_roles": load_roles,
    "load_users": load_users,
    "load_projects": load_projects,
    "clear": clear
}


class DevResource(Resource):
    def post(self):
        action = request.json.get('action')

        handler = actions.get(action)
        if handler:
            handler()

        return {
            'status': 'success',
            'action': action,
        }
