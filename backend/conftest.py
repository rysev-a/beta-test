import pytest
from app import create_app


@pytest.fixture(scope='session')
def app():
    app = create_app('app.settings.test')
    return app


@pytest.fixture
def client(app):
    with app.test_client() as c:
        yield c
